let AccountRepository = module.exports = {}
let User = require('../Models/User');

AccountRepository.findAllUsers = async function () {
    let result = await User.find();
    return result;
};

AccountRepository.findUserByEmailOrUsername = async function (user) {
    let result = await User.findOne({
        $or: [
            { email: user.email },
            { username: user.username },
        ]
    });
    return result;
};

AccountRepository.findUserByIdentity = async function (identity) {
    let result = await User.findOne({
        $or: [
            { email: identity },
            { username: identity },
        ]
    });
    return result;
};

AccountRepository.createUser = async function (user) {
    let result = await User.create({ email: user.email, username: user.username, verificationCode: user.verificationCode });
    return result;
};

AccountRepository.findUserByVerificationCode = async function (verificationCode) {
    let result = await User.findOne({ verificationCode: verificationCode });
    return result;
};

AccountRepository.findUserByEmail = async function (email) {
    let user = await User.findOne({ email: email });
    return user;
};

AccountRepository.setPassword = async function (userId, password) {
    let result = await User.updateOne({ _id: userId }, { $set: { password: password, verificationCode: '', verified: true } });
    return result;
};

