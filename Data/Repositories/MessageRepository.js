let MessageRepository = module.exports = {}
let Message = require('../Models/Message');

MessageRepository.findAllMessages = async function () {
    let result = await Message.find().sort({ MessageDate: -1 });
    return result;
};

MessageRepository.createMessage = async function (message) {
    let result = await Message.create(
        {
            textMessage: message.textMessage,
            userId: message.userId,
            messageDate: message.messageDate,
            username: message.username
        }
    );
    return result;
};

MessageRepository.DeleteMessages = async function () {
    let result = await Message.deleteMany();
    return result;
};
