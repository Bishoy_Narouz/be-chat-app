let UserRepository = module.exports = {}
let User = require('../Models/User');

UserRepository.findAllUsers = async function () {
    let result = await User.find();
    return result;
};

UserRepository.findAllFriends = async function (id) {
    let result = await User.find(
        {
            $and: [
                { verified: true },
                { _id: { $ne: id } }

            ]
        }
    ).select({ _id: 0, username: 1, email: 1 });
    return result;
};

UserRepository.updateUser = async function (id, verificationCode) {
    let result = await User.updateOne({ _id: id }, { $set: { verificationCode: verificationCode } });
    return result;
};

UserRepository.DeleteUsers = async function () {
    let result = await User.deleteMany();
    return result;
};
