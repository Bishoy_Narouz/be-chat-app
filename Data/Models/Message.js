var mongoose = require('mongoose');

var Message = new mongoose.Schema({
    textMessage: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    messageDate: {
        type: Date,
        require: true
    }
});

var Message = mongoose.model('Message', Message);
module.exports = Message; 