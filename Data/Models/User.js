var mongoose = require('mongoose');

var User = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: false
    },
    verified: {
        type: Boolean,
        required: false,
        default: false
    },
    verificationCode: {
        type: String,
        required: false
    }
});

// Compile model from schema
var User = mongoose.model('User', User);
module.exports = User; 