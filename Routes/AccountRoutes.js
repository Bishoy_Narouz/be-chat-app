let AccountService = require('../Domain/AccountService');
let express = require('express')
    , router = express.Router()
let errorCode = require('../Enums/ErrorCodeEnum');
const jwt = require('jsonwebtoken');
const config = require('../config');

router.post('/Login', async function (req, res) {
    try {
        let user = {
            identity: req.body.identity,
            password: req.body.password
        }
        let result = await AccountService.Login(user);
        let token = jwt.sign({ Id: result._id }, config.Secret, { expiresIn: '30m' });
        result.token = token;
        res.send({
            Success: true,
            Message: '',
            Data: result
        });

    } catch (e) {
        if (e.message === errorCode.INVALID_CREDENTIALS) {
            res.send({ Success: false, Message: errorCode.INVALID_CREDENTIALS });
        }
        else if (e.message === errorCode.ACCOUNT_IS_NOT_VERIFIED) {
            res.send({ Success: false, Message: errorCode.ACCOUNT_IS_NOT_VERIFIED });
        }
        else {
            res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
        }
    }
});

router.post('/Register', async function (req, res) {
    try {
        let user = {
            email: req.body.email,
            username: req.body.username
        }
        let result = await AccountService.Register(user);
        res.send({ Success: true, Message: 'Registered Successfully' });

    } catch (e) {
        if (e.message === errorCode.USER_HAS_REGISTERED_BEFORE) {
            res.send({ Success: false, Message: errorCode.USER_HAS_REGISTERED_BEFORE });
        } else {
            res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
        }
    }
});

router.post('/SetPassword', async function (req, res) {
    try {
        let result = await AccountService.SetPassword(req.body.verificationCode, req.body.password);
        res.send({ Success: true, Message: 'Password has Set Successfully' });

    } catch (e) {
        if (e.message === errorCode.INVALID_VERIFICATION_CODE) {
            res.send({ Success: false, Message: errorCode.INVALID_VERIFICATION_CODE });
        } else {
            res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
        }
    }
});

router.post('/ResendVerificationEmail', async function (req, res) {
    try {
        let result = await AccountService.ResendVerificationEmail(req.body.email);
        res.send({ Success: true, Message: 'Email has sent successfully' });

    } catch (e) {
        if (e.message === errorCode.INVALID_EMAIL) {
            res.send({ Success: false, Message: errorCode.INVALID_EMAIL });
        } else if (e.message === errorCode.VERIFIED_ACCOUNT) {
            res.send({ Success: false, Message: errorCode.VERIFIED_ACCOUNT });
        } else {
            res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
        }
    }
});

module.exports = router