const bodyParser = require('body-parser');
const AccountRoutes = require('./AccountRoutes');
const UserRoutes = require('./UserRoutes');
const MessageRoutes = require('./MessageRoutes');
const cors = require('cors')

module.exports = function (app, config) {
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cors(config.corsOptions));
    app.use('/api/Account', AccountRoutes);
    app.use('/api/User', UserRoutes);
    app.use('/api/Message', MessageRoutes);
}

