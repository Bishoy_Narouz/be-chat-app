let UserService = require('../Domain/UserService');
let express = require('express')
    , router = express.Router()
let errorCode = require('../Enums/ErrorCodeEnum');

router.get('/findAllUsers', async function (req, res) {
    try {
        let result = await UserService.findAllUsers();
        res.send({ Success: true, Message: '', Data: result });

    } catch (e) {
        res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
    }
})

router.get('/findAllFriends/:currentUserId', async function (req, res) {
    try {
        let result = await UserService.findAllFriends(req.params.currentUserId);
        res.send({ Success: true, Message: '', Data: result });

    } catch (e) {
        res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
    }
})

router.put('/updateUser', async function (req, res) {
    try {
        let result = await UserService.updateUser(req.body.id, req.body.verificationCode);
        res.send({ Success: true, Message: 'User Updated successfully' });

    } catch (e) {
        res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
    }
})

router.delete('/deleteUsers', async function (req, res) {
    try {
        let result = await UserService.DeleteUsers();
        res.send({ Success: true, Message: 'Users Deleted Successfully' });

    } catch (e) {
        res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
    }
})

module.exports = router