let MessageService = require('../Domain/MessageService');
let express = require('express')
    , router = express.Router();
let errorCode = require('../Enums/ErrorCodeEnum');

router.get('/GetAllMessages', async function (req, res) {
    try {
        let result = await MessageService.findAllMessages();
        res.send({ Success: true, Message: '', Data: result });

    } catch (e) {
        res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
    }
})

router.post('/createMessage', async function (req, res) {
    try {
        let message = {
            textMessage: req.body.textMessage,
            userId: req.body.userId
        }
        let result = await MessageService.createMessage(message);
        res.send({ Success: true, Message: 'Message Craeted Successfully' });

    } catch (e) {
        res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
    }
})

router.delete('/deleteMessages', async function (req, res) {
    try {
        let result = await MessageService.DeleteMessages();
        res.send({ Success: true, Message: 'Messages Deleted Successfully' });

    } catch (e) {
        res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
    }
})

module.exports = router;