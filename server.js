const express = require('express');
const app = express();
const config = require('./config');
const db = require('./Data/DBConfig');
const APIs = require('./Routes/APIs')(app, config);
const socketIoConfig = require('./Services/SocketIOService')(app, config);


app.listen(config.BackendPort, () => console.log(`the app listening on port ${config.BackendPort}`));
