const sgMail = require('@sendgrid/mail');
const config = require('../config');

module.exports = function (email, username, verificationCode) {
    sgMail.setApiKey(config.SendGridKey);
    let url = config.FrontendUrl + 'setPassword/' + verificationCode;
    const msg = {
        to: email,
        // to:'bishoynarouz92@gmail.com',
        from: 'bishoy@testApp.com',
        subject: 'chat app',
        text: `thanks ${username} please click the link below to verfiy your account :`,
        html: `<div>
				<h1> Hello ` + username + `</h1>
				<p>Please Click the link to verify your Account : </p>
				<p>`+ url + `</p>
				<a href="`+ url + `">click here</a>
				</div>`
    };
    return new Promise(function (resolve, reject) {
        sgMail.send(msg);
        resolve(true);
    });

}