let MessageRepository = require('../Data/Repositories/MessageRepository');
let http = require('http');
let socketIO = require('socket.io');

module.exports = function (app, config) {
    let server = http.Server(app);
    let io = socketIO(server);

    io.on('connection', (socket) => {
        socket.on('new-message', async (message) => {
            MessageRepository.createMessage(message);
            io.emit('new-message', message);
            console.log(message);
        });
    });

    server.listen(config.SocketPort, () => console.log(`socket working on port ${config.SocketPort}`));
};