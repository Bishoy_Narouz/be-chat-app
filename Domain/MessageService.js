const MessageService = module.exports = {}
const MessageRepository = require('../Data/Repositories/MessageRepository');

MessageService.findAllMessages = async function () {
    let messages = await MessageRepository.findAllMessages();
    return messages;
}

MessageService.createMessage = async function (message) {
    let result = await MessageRepository.createMessage(message);
    return result;
}

MessageService.DeleteMessages = async function () {
    await MessageRepository.DeleteMessages();
    return;
}
