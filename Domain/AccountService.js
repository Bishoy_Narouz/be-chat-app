const AccountService = module.exports = {}
const AccountRepository = require('../Data/Repositories/AccountRepository');
const SendMailService = require('../Services/SendMailService');
const randomstring = require('randomstring');
const errorCode = require('../Enums/ErrorCodeEnum');
const bcrypt = require('bcrypt');

AccountService.Login = async function (model) {
    let user = await AccountRepository.findUserByIdentity(model.identity);
    if (user && user.verified === false) {
        throw new Error(errorCode.ACCOUNT_IS_NOT_VERIFIED);
    } else if (!user || !bcrypt.compareSync(model.password, user.password)) {
        throw new Error(errorCode.INVALID_CREDENTIALS);
    }
    else {
        return { _id: user._id, username: user.username, email: user.email };
    }
};

AccountService.Register = async function (model) {
    let user = await AccountRepository.findUserByEmailOrUsername(model);
    if (user) {
        throw new Error(errorCode.USER_HAS_REGISTERED_BEFORE);
    } else {
        model.verificationCode = randomstring.generate(15);
        await AccountRepository.createUser(model);
        await SendMailService(model.email, model.username, model.verificationCode);
        return;
    }
};

AccountService.SetPassword = async function (verificationCode, password) {
    let user = await AccountRepository.findUserByVerificationCode(verificationCode);
    if (!user) {
        throw new Error(errorCode.INVALID_VERIFICATION_CODE);
    } else {
        password = bcrypt.hashSync(password, 15);
        await AccountRepository.setPassword(user._id, password);
        return;
    }
};

AccountService.ResendVerificationEmail = async function (email) {
    let user = await AccountRepository.findUserByEmail(email);
    if (!user) {
        throw new Error(errorCode.INVALID_EMAIL);
    } else if (user.verified === true) {
        throw new Error(errorCode.VERIFIED_ACCOUNT);
    } else {
        await SendMailService(user.email, user.username, user.verificationCode);
        return;
    }
};






