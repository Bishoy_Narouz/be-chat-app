const UserService = module.exports = {}
const UserRepository = require('../Data/Repositories/UserRepository');

UserService.findAllUsers = async function () {
    let users = await UserRepository.findAllUsers();
    return users;
}

UserService.findAllFriends = async function (id) {
    let users = await UserRepository.findAllFriends(id);
    return users;
}

UserService.updateUser = async function (id, verificationCode) {
    let user = await UserRepository.updateUser(id, verificationCode);
    return user;
}

UserService.DeleteUsers = async function () {
    await UserRepository.DeleteUsers();
    return;
}
